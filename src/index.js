var amqp = require('amqplib/callback_api');
var conn = null;


module.exports = function amqpPlugin(schema, options){

  // Connect to rabbit and store connection in global variable
  amqp.connect(options.url, (_err, _conn) => {
    conn = _conn;
  });

  // Post save hook
  schema.post('save', function(){
    // Build payload to match nestjs requirements
    const _data = {
      pattern: 'createBlock',
      data: this
    };
    publish(options.queue + '.update', JSON.stringify(_data));
  });

  // Post updateOne hook
  schema.post('updateOne', function(){
    // Build payload to match nestjs requirements
    const _data = {
      pattern: 'createBlock',
      data: this
    };
    publish(options.queue + '.update', JSON.stringify(_data));
  });
  
  // Post remove hook
  schema.post('remove', function(){
    // Build payload to match nestjs requirements
    const _data = {
      pattern: 'createBlock',
      data: this
    };
    publish(options.queue + '.remove', JSON.stringify(_data));
});

};

// Publish
function publish(queue, message) {
  wait_connection();

  conn.createChannel((err, ch) => {
    if (err != null) bail(err);
    ch.assertQueue(queue);
    ch.sendToQueue(queue, Buffer.from(message));
    ch.close();
  });
}

// Print error and exit
function bail(err) {
  console.error(err);
  process.exit(1);
}

// Check connection and wait for it
function wait_connection() {
  // Store attempts count
  let attempts = 0;

  // Check connection
  if (conn == null) {

    // Check attempts count
    if (attempts < 360) {

      // wait and update attempts counter
      setTimeout(() => {
        attempts++;
      }, 500)
    } else {

      // Exit
      bail('Could not get a connection to message broker')
    }
  }
}